var string = '{\
	"musicApp":[\
	{\
		"id":1,\
		"nombre": "Californication",\
		"artista": "Red Hot Chili Peppers",\
		"album" : "Californication",\
		"uri": "./assets/californication.mp3",\
		"caratula":"./img/RedHotChiliPeppersCalifornication.jpg"\
	},\
	{\
		"id":2,\
		"nombre": "Clint Eastwood",\
		"artista": "Gorillaz",\
		"album" : "Clint Eastwood",\
		"uri": "./assets/Clint Eastwood.mp3",\
		"caratula":"./img/gorillazCE.jpg"\
	},\
	{\
		"id":3,\
		"nombre": "Clocks",\
		"artista": "Coldplay",\
		"album" : "Clocks",\
		"uri": "./assets/Clocks.mp3",\
		"caratula":"./img/coldplay.jpg"\
	},\
	{\
		"id":4,\
		"nombre": "Come As You Are",\
		"artista": "Nirvana",\
		"album" : "Come As You Are",\
		"uri": "./assets/ComeAsYouAre.mp3",\
		"caratula":"./img/Nirvana.jpg"\
	}\
	]\
}';

var objJSON = JSON.parse(string);
console.log(objJSON);

var musicApp = objJSON.musicApp;
console.log(Object.keys(musicApp).length);



function cargarApp(){

	llenarLista(musicApp);
	
}

function llenarLista(musicList){

	var lista = document.getElementById('lista');

	for (var i = 0; i < Object.keys(musicList).length; i++) {
		lista.innerHTML += '<div class="song" data-uri="'+musicList[i].uri+'" data-id="'+musicList[i].id+'" data-cover="'+musicList[i].caratula+'" data-nombre="'+musicList[i].nombre+'" data-artista="'+musicList[i].artista+'" data-album="'+musicList[i].album+'">'+musicList[i].nombre+' - '+musicList[i].artista+'</div>';
		
	}


	var songs = lista.getElementsByClassName('song');
	var player = document.getElementById("player").getElementsByTagName('audio')[0];
	player.setAttribute("data-id",musicList[0].id);
	var cover = document.getElementById("player").getElementsByTagName("img")[0];
	
	console.log(musicList[0].id);

	var tit = document.getElementById("info").getElementsByClassName("contenido")[0];
	var art = document.getElementById("info").getElementsByClassName("contenido")[1];
	var alb = document.getElementById("info").getElementsByClassName("contenido")[2];
	var titulo = document.getElementsByTagName("title");
	console.log(player);





	//-------------------Imagen Pausa---------------------------

	cover.onclick= function(){
		//console.log("CLick Imagen");
		if (player.paused)
			player.play();
		else
			player.pause();
	}
	

	//-------------------Cancion Inicial------------

	document.title= "♪♫ "+musicList[0].nombre;
	cover.src = musicList[0].caratula;
	player.src = musicList[0].uri;
	tit.innerHTML = musicList[0].nombre;
	art.innerHTML = musicList[0].artista;
	alb.innerHTML = musicList[0].album;
	



	//----------------------------------------------

	for (var i = 0; i < songs.length; i++) {
		songs[i].onclick = function(){ 
			
			
			
			
			cover.src = this.getAttribute("data-cover");
			player.setAttribute("data-id", this.getAttribute("data-id"));
			player.src = this.getAttribute("data-uri");
			tit.innerHTML = this.getAttribute("data-nombre");
			art.innerHTML = this.getAttribute("data-artista");
			alb.innerHTML = this.getAttribute("data-album");

			document.title = "♪♫ "+ this.getAttribute("data-nombre");

			

			player.play();
		//console.log(player);


		};




	};



	//console.log(player);
	

	




	player.onended = function(){

		console.log(player);

		//var id = parseInt(player.getAttribute("data-id"));

		var songs = lista.getElementsByClassName('song');
		//console.log(id);
		console.log(songs.length);
		
		for (var i = 0; i < songs.length; i++) {
			var id = parseInt(player.getAttribute("data-id"));
			
			console.log(songs[i].dataset.id ==(id+1));
			console.log(songs[i].dataset.id + " - " + (id+1));

			if(songs[i].dataset.id == (id+1)){
				player.src = songs[i].dataset.uri;
				cover.src = songs[i].dataset.cover;
				tit.innerHTML = songs[i].dataset.nombre;
				art.innerHTML = songs[i].dataset.artista;
				alb.innerHTML = songs[i].dataset.album;
				document.title = "♪♫ "+ songs[i].dataset.nombre;
				player.play();
				//i= songs.length;
				//console.log(i);
			}
							
		};		

	};


	


}

